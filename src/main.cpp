#include <stdio.h>
#include <windows.h>

#include "TSFuncs.hpp"

ADDR currentShapeBase;
char currentShapeBaseID[16];

BlFunctionDef(void, __thiscall, ShapeBase__processTick, ADDR, ADDR);
BlFunctionHookDef(ShapeBase__processTick);

void __fastcall ShapeBase__processTickHook(ADDR obj, void *blank, ADDR move)
{
	if (!(*(ADDR *)(obj + 68) & 2)) {
		currentShapeBase = obj;
		snprintf(currentShapeBaseID, 16, "%d", *(ADDR *)(obj + 32));
		ShapeBase__processTickOriginal(obj, move);
	}
	else
		ShapeBase__processTickOriginal(obj, move);
}

BlFunctionDef(void, __cdecl, SimpleQueryList__insertionCallback, ADDR *, void *);
BlFunctionHookDef(SimpleQueryList__insertionCallback);

void __cdecl SimpleQueryList__insertionCallbackHook(ADDR *sceneObj, void *key)
{
	if (currentShapeBase) {
		char colID[16];
		snprintf(colID, 16, "%d", *(ADDR *)((ADDR)sceneObj + 32));

		const char *ret = tsf_BlCon__executef(3, "onObjectCollisionTest",
		                                      currentShapeBaseID, colID);

		if (ret[0] == '0' && !ret[1])
			return;
	}

	SimpleQueryList__insertionCallbackOriginal(sceneObj, key);
}

bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	BlScanFunctionHex(ShapeBase__processTick, "55 8B EC 83 E4 C0 81 EC ? ? ? ? A1 ? ? ? ? 33 C4 89 84 24 ? ? ? ? 8B 45 08 53 8B D9");
	BlScanFunctionHex(SimpleQueryList__insertionCallback, "56 8B 74 24 0C 8D 4E 04");

	BlCreateHook(ShapeBase__processTick);
	BlCreateHook(SimpleQueryList__insertionCallback);

	BlTestEnableHook(ShapeBase__processTick);
	BlTestEnableHook(SimpleQueryList__insertionCallback);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");

	return true;
}

bool deinit()
{
	BlTestDisableHook(ShapeBase__processTick);
	BlTestDisableHook(SimpleQueryList__insertionCallback);

	BlPrintf("%s: deinit'd", PROJECT_NAME);

	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
