# Object Collision
A DLL that lets you control collision between individual objects.

## Usage
The DLL will call the TorqueScript method `onObjectCollisionTest(%obj, %col)` which you must define in your script.  Returning `true` or an empty string will make collision work as normal.  Returning `false` will disable collision between the two objects.

## Behavior
This DLL will prevent objects from being added to an object's working collision set.  However, if an object is already in another object's working collision set, it will remain there until the objects move away from each other.  This means that if two objects are currently colliding and you disable collision between them, they will continue to collide until they move away from each other.

## Examples
This example will allow admins to walk through any bricks with the name "adminOnly" while non-admins cannot.
```cs
function onObjectCollisionTest(%obj, %col)
{
	if (%col.getType() & $TypeMasks::FxBrickAlwaysObjectType
	&&  %obj.getType() & $TypeMasks::PlayerObjectType) {
		if (%col.getName() $= "_adminOnly" && %obj.client.isAdmin)
			return false;
	}

	return true;
}
```
<br>

This example will let players on the same Slayer team walk through each other.
```cs
function onObjectCollisionTest(%obj, %col)
{
	if (%obj.getType() & $TypeMasks::PlayerObjectType
	&&  %col.getType() & $TypeMasks::PlayerObjectType) {
		if (isObject(%obj.client.slyrTeam)
		&&  %obj.client.slyrTeam == %col.client.slyrTeam) {
			return false;
		}
	}

	return true;
}
```
